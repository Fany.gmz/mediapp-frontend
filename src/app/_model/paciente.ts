//Representar la estructura en Json, que va devolver el servicio (BackEnd)
//Export usar la clase en otras clases, similar pubic 
export class Paciente {
    idPaciente: number;
    nombres: string;
    apellidos:string;
    dui:string;
    direccion:string;
    telefono:string;
}