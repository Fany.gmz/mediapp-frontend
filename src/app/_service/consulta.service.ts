import { ConsultaResumenDTO } from './../_dto/consultaResumenDTO';
import { FiltroConsultaDTO } from './../_dto/filtroConsultaDTO';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { ConsultaListaExamenDTO } from '../_dto/consultaListaExamenDTO';
import { Consulta } from '../_model/consulta';

@Injectable({
  providedIn: 'root'
})
export class ConsultaService {

  url: string = environment.HOST+'/consultas';

  constructor(private http: HttpClient) { }

  registrar(consultaDTO: ConsultaListaExamenDTO) {
    return this.http.post(this.url,consultaDTO);
  }

  buscar(filtroConsultaDTO: FiltroConsultaDTO){
    return this.http.post<Consulta[]>(this.url+ '/buscar',filtroConsultaDTO)
  }

  listarExamenPorConsulta(idConsulta:number){
    return this.http.get<ConsultaListaExamenDTO[]>(`${this.url}/consultaexamenes/${idConsulta}`);
  }

  listarResumen(){
    return this.http.get<ConsultaResumenDTO[]>(`${this.url}/listarResumen`);
  }
  genererReporte(){
    return this.http.get(`${this.url}/generarReporte`, {
      responseType: 'blob' //es dcir un arreglo de bytes
    });
  }

  guardarArchivo(data : File){
    let formdata : FormData = new FormData();
    formdata.append('file', data);    

    return this.http.post(`${this.url}/guardarArchivo`, formdata, {
      responseType: 'text'
    });
  }

  leerArchivo(){
    return this.http.get(`${this.url}/leerArchivo/1`, {
      responseType: 'blob'
    });
  }
}
