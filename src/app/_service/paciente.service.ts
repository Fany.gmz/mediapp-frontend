import { Paciente } from './../_model/paciente';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PacienteService {
  //Injeccion de independencia se hace en el constructor...
  //Se debe habilitar activar las peticiones ( el modulo) HTTP. Angular es un Framework MOdular
  url:string = environment.HOST +'/pacientes';

  pacienteCambio = new Subject<Paciente[]>();//Programacion reactiva, tiene un objeto en estudio
  mensajeCambio = new Subject<string>();//msj edit/inset hasta el padre
  constructor(private http:HttpClient) { }

 listar() {
   //apuntando con la ruta del servio backeEnd
   //Angular es independiente de que Lenguaje se halla realizado el backEnd, lo que necesita es La URL que apunte al recurso solicitado.
   return this.http.get<Paciente[]>(this.url);
 }

 listarPorId(idPaciente: number) {
  return this.http.get<Paciente>(`${this.url}/${idPaciente}`);
}

listarPageable(p: Number,s:number){
  return this.http.get<any>(`${this.url}/pageable?page=${p}&size${s}`);
                  // any porque ese servicio returna una estructa indepentiente, no tenfo un tipo defino 
}
registrar(paciente: Paciente) {
  return this.http.post(this.url, paciente);
}

modificar(paciente: Paciente) {
  return this.http.put(this.url, paciente);
}

eliminar(idPaciente: number) {
  return this.http.delete(`${this.url}/${idPaciente}`);
}
}
