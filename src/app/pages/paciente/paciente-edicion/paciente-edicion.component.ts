import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { PacienteService } from 'src/app/_service/paciente.service';
import { Paciente } from 'src/app/_model/paciente';

@Component({
  selector: 'app-paciente-edicion',
  templateUrl: './paciente-edicion.component.html',
  styleUrls: ['./paciente-edicion.component.css']
})
export class PacienteEdicionComponent implements OnInit {
  form: FormGroup;
  id: number;
  edicion: boolean;
  //Recuperar data de la URL activatedRoute (url activa) 
  //router: Router => permite navegar de forma progamatica
  constructor(private route: ActivatedRoute, private router: Router, private pacienteService: PacienteService) { }
  //Al cargar el elemento(Pagina) los input deben estar vacios.Representar los mismo datos que tiene el form html
  ngOnInit() {
    this.form = new FormGroup({
      'id': new FormControl(0),
      'nombres': new FormControl(''),
      'apellidos': new FormControl(''),
      'dni': new FormControl(''),
      'direccion': new FormControl(''),
      'telefono': new FormControl('')
    });
    //subscribirse al v. route  y Capturar Variable de la URL (funcion flrcha permite hacer una accion)
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];//capturar la variable de la url ID, definita en la url.module
      this.edicion = params['id'] != null;//validacion
      this.initForm();
    });

  }

  initForm() {
    if (this.edicion) {
      //cargar la data del servicio en el form
      this.pacienteService.listarPorId(this.id).subscribe(data => {
        this.form = new FormGroup({
          'id': new FormControl(data.idPaciente),
          'nombres': new FormControl(data.nombres),
          'apellidos': new FormControl(data.apellidos),
          'dni': new FormControl(data.dui),
          'direccion': new FormControl(data.direccion),
          'telefono': new FormControl(data.telefono)
        });
      });
    }
  }

  operar() {
    let paciente = new Paciente();
    paciente.idPaciente = this.form.value['id'];
    paciente.nombres = this.form.value['nombres'];
    paciente.apellidos = this.form.value['apellidos'];
    paciente.dui = this.form.value['dni'];
    paciente.direccion = this.form.value['direccion'];
    paciente.telefono = this.form.value['telefono'];

    if (this.edicion) {
      //servicio de edicion
      this.pacienteService.modificar(paciente).subscribe(() => {
        this.pacienteService.listar().subscribe(data => {
          this.pacienteService.pacienteCambio.next(data);
          this.pacienteService.mensajeCambio.next('SE MODIFICO');
        });
      });
    } else {
      //servicio de registro
      /*
      * Luego de insertar, se subscribe el metodo listar (se guarda en una variable reactiva)
      * VARIABLE REACTIVA -> guarda la nueva data, y se pinta en la vista solo si se actualizan o asiganda data. se utiliza en la capa padre
      */
      this.pacienteService.registrar(paciente).subscribe(() => {
        this.pacienteService.listar().subscribe(data => {
          this.pacienteService.pacienteCambio.next(data);//variable reactiva, tiene la nueva  data despues de insertar
          this.pacienteService.mensajeCambio.next('SE REGISTRO');
        });
      });
    }
    //Despues de INSERT OR UPDATE NAVEGAR HASTA /PACIENTE
    this.router.navigate(['paciente']);

  }
}
