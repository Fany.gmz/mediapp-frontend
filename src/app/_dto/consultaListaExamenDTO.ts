import { Examen } from './../_model/examen';
import { Consulta } from './../_model/consulta';
//Representar la info de la tabla trasaccional
export class ConsultaListaExamenDTO{
    consulta: Consulta;
    listExamen: Examen[];
}