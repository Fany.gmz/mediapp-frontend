/* 
Representa la estructura del form que quiero enviar.
*/
export class FiltroConsultaDTO{
    dui: string;
    nombreCompleto:string;
    fechaConsulta:Date;// a nivel de backEnd LocalDateTime, se transforma el Date en String formato ISO.
    constructor(dui:string, nombreCompleto:string,fechaConsulta:Date){
        this.dui = dui;
        this.nombreCompleto = nombreCompleto;
        this.fechaConsulta = fechaConsulta;
    }
}