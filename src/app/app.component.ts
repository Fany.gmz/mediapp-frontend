import { MenuService } from './_service/menu.service';
import { LoginService } from './_service/login.service';
import { Component, OnInit } from '@angular/core';
import { Menu } from './_model/menu';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  implements OnInit {
  //Inyectar servicio para
  menus: Menu[] = [];
  constructor(public loginService : LoginService, private MenuService : MenuService) {
  }
  ngOnInit() {
    this.MenuService.menuCambio.subscribe(data => {
      this.menus = data;
    });
  }
}
